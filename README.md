# electron, ts, cra boilerplate

---

#### 이 repository는 프론트엔드 웹 개발자 jo seung hyun(조승현)이 작성한

##### typescript 기반 electron boilerplate 입니다.

[boilerplate](https://bitbucket.org/joe901220/next-client-boilerplate/src/master/)

---

## Skill Stack:

#### 1) electron, react(cra) 기반

#### 2) Front library: React, Typescript

#### 3) State library: Mobx

#### 4) Style: Styled-Components, CSS

#### 5) Test: Enzyme, Jest(예정)

---

## issue

#### 1) customize-cra를 적용하기 위해 public -> electron.js에서 nodeIntegration: true 추가

---

## To do list

#### 1)
