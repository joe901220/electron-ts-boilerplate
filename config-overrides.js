const { addDecoratorsLegacy, disableEsLint, override, setWebpackTarget } = require("customize-cra")

module.exports = {
  webpack: override(disableEsLint(), addDecoratorsLegacy(), setWebpackTarget("electron-renderer"))
}
