import React from "react"
import { Provider, observer } from "mobx-react"
import { HashRouter as Router } from "react-router-dom"
import { ThemeProvider } from "styled-components"

import Routes from "Routes"
import GlobalStyles from "config/styles/GlobalStyles"
import rootStore from "stores/RootStore"
import themes from "config/styles"

const App: React.FC = observer(() => {
  return (
    <Provider {...rootStore}>
      <GlobalStyles />
      <ThemeProvider theme={themes}>
        <Router>
          <Routes />
        </Router>
      </ThemeProvider>
    </Provider>
  )
})

export default App
