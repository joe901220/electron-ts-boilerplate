import UserStore from "./UserStore"

class RootStore {
  userStore: any
  constructor() {
    this.userStore = new UserStore(this)
  }
}

export default new RootStore()
