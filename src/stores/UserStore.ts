import { observable, action } from "mobx"

import { IUserModel } from "./models/UserModel"

export interface IUserStore {
  root: any
  users: IUserModel
}

class UserStore {
  @observable root: any
  @observable users: any[]
  constructor(root?: any) {
    this.root = root
    this.users = []
  }
}

export default UserStore
